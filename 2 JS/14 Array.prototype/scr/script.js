/* 
Создайте метод который будет умножать элементы массива на то число которое будет передавать
пользователь.
 */
var flag = false;
//создаю массив на 5 элементов
var myArray = new Array(5);
//заполняю случайными числами
myArray = jQuery.map(myArray, function (el) {
  return el = Math.round(Math.random() * 100);
})
console.log(myArray);

//создаю инпут
$('body')
  .append('<main class = "container"><form><div class = "col-md-2"><label for="userNumber">Enter number</label><input class="form-control" type = "number" id = "userNumber"></div> </form></main>')

//Вывожу на экран исходный , заполненный рандомно массив
$('form').after($('<p>').text(`Source array: ${myArray.join('\n')}`))

//Событие на поле ввода.
$('#userNumber').change(function () {
  if (flag){
  $('p:first').text(`Source array: ${myArray.join('\n')}`)
  }
  $(this) //при вводе значения создаю кнопку 
    .after($('<div><input class="btn btn-primary" type="button" value="start"></div>')//вывожу кнопку на экран
      .click(function () {  // событие на кнопку


        //вывожу значение на которое будет умножение
        $('p:first')
          .after($('<p>').text(`Array elements multiplied by: ${$('#userNumber').val()}`))


        //метод принимает исходный массив и значение на которое будет умножение эелементов.
       myArray.multiplicationArr($('#userNumber').val());

        $('#userNumber').val(''); // убираю введенное значение
        $(this).remove();//убираю кнопку

        //Вывожу на экран полученный массив
        $('p:nth-of-type(2)').after($('<p>').text(`Array after multiplications: ${myArray.join('\n')}`))

        flag = true;

        //Удаляю предидущий результат.
        $('p:nth-of-type(4),p:nth-of-type(5)').remove();
      }))
})

/* Сделайте так, чтобы метод наследовался каждым массивом подобно методу pop(). */
Array.prototype.multiplicationArr = function multiplicationArr(number) {
  for (let i = 0; i < this.length; i++) {
    let element = this[i] * number;
    this[i] = element;
  }
};
