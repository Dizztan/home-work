//Событие на первую кнопку
document.getElementById("startBtn").onclick = (start) => {
  //Переменные для записи созданных элементов.
  const inp = document.createElement("input"),
    btn = document.getElementById("startBtn");

  //вывожу вместо стартовой кнопки input.
  btn.replaceWith(inp);

  //задаю атрибуты.
  inp.setAttribute("placeholder", "Диаметр круга");
  inp.setAttribute("type", "number");
  inp.setAttribute("id", "inDiam");

  //Вешаю событие на inp.
  document.getElementById("inDiam").onchange = (EnterDiam) => {
    //Переменные для проверки длины и записи вводимых данных.
    var picBtn,
      valueInp = document.getElementById("inDiam").value;
    //Проверка длины введеных данных.
    if (inp.value.length < 3) {
      //Если все ок создаю кнопку для вывода результата на экран с учетом введенных данных.
      picBtn = document.createElement("input");
      //задаю атрибуты
      picBtn.setAttribute("type", "button");
      picBtn.setAttribute("value", "Нарисовать");
      picBtn.setAttribute("id", "startPicBtn");

      //Вывожу на экран кнопку.
      inp.insertAdjacentElement('afterend', picBtn);

    } else {
      alert('Введите двузначное число.');
    }

    //Событие для кнопки "Нарисовать".
    document.getElementById("startPicBtn").onclick = (showPic) => {
      //создаю общий див main.
      const main = document.createElement("main");
      //Задаю класс для обработки в CSS.
      main.setAttribute("class", "show__pic");
      //Убираю input.
      inp.remove();
      //Заменяю кнопку "Нарисовать на main".
      picBtn.replaceWith(main);

      const arr = new Array();

      //Заполняю массив div'ами задаю стили.
      for (let i = 0; i < 100; i++) {
        arr[i] = document.createElement('div');
      }

      //Задаю случайный цвет и стили с учетом введенных данных (размеров) пользователем.
      arr.map((element) => {
        element.style.width = valueInp + "px";
        element.style.height = valueInp + "px";
        element.style.borderRadius = (valueInp / 2) + "px";
        element.style.backgroundColor = '#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase();
        //Вывод на кран
        main.insertAdjacentElement('afterbegin', element);
      });

      //Событие на элементы массива.
      arr.forEach(item => {
        item.addEventListener('click', () => {
          item.remove();
        });
      });
    }
  }
}



