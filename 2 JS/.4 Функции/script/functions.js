// Классная работа 

var a, b, value1, value2, sign, result, age, before = "до", after = "после", i, years;

var btn = document.querySelector('input[type=button][value=calculator]');



//Функция сложения.
const add = (a, b) => {
  return a + b;
}

//Функция умножения.
const multiple = (a, b) => {
  return a * b;
}

//Функция деления.
const division = (a, b) => {
  if (a == 0 || b == 0) {
    return alert("ошибка, значение не должно быть равно 0");
  } else {
    return a / b;
  }
}


//Функция вычитания.
const subtraction = (a, b) => {
  return a - b;
}


//Функция калькулятор.
function calculate(a, b, callback) {
  if (arguments.length != 3) {
    console.log(`Количество аргументов должно быть равно 3 ! Вы ввели ${arguments.length} `);
  } else {
    result = callback(a, b);
  }
}



//Ввод и проверка данных.
function input() {
  value1 = parseInt(prompt("Введите первое число", ""));
  sign = prompt("Введите знак операции -, +, *, /", "");
  value2 = parseInt(prompt("Введите второе число", ""));
}

function examNan() {
  value1 = isNaN(value1) ? 0 : value1;
  value2 = isNaN(value2) ? 0 : value2;
}


const exam = () => {
  switch (sign) {
    case '+':
      calculate(value1, value2, add);
      break;

    case "*":
      calculate(value1, value2, multiple);
      break;

    case "-":
      calculate(value1, value2, subtraction);
      break;

    case "/":
      calculate(value1, value2, division);
      break;
    default:
      alert(`${sign} - Не является арифметическим знаком.`);
  }
}

//Функция вывода результата
function show() {
  document.getElementById("result").innerHTML = `<p> ${value1} ${sign} ${value2} = ${result} </p>`;
}


function start() {
  input();
  examNan();
  exam();
  calculate();
  result = result === undefined ? alert('Ошибка ввода данных,введите другие данные') : show();
}

btn.onclick = start;






//Домашняя  работа


//Переписать функцию используя оператор "?" или "||".
/* if (age > 18) {
  return true;
} else {
  return confirm('Родители разрешили?');
} */


//Переписал функцию. Проверка введенных  данных + вывод.
function checkAge(age) {
  age = (age > 18) ? true : confirm('Родители разрешили?');
  document.getElementById("allowed").innerHTML = `${age}`;
}

//ввод данных
function inputAge() {
  years = parseInt(prompt("Сколько лет", ""));
}



btn = document.querySelector('input[type=button][value=beer]');
btn.onclick = check;

function check() {
  inputAge();
  checkAge(years);
}

//Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.


//Создаю и заполняю масив.
const arr = [1, 2, 55, 96, 47, 3544, -276, 445, 3, 6, 9, 85];

//Создаю массив в который будут записываться изменения.
var newArr = [];

//Функция map
const map = (array, fn) => {
  return fn(array);
}

//Функция для обработки массива
const fn = (arg) => {
  for (i = 0; i < arg.length; i++) {
    newArr[i] = arg[i] * 2;
  }
  return newArr;
}


//функция вывода 
function showRes(treatment, array) {
  document.write(`<p> Массив ${treatment} обработки: <br> ${array.join(". ")} </p>`);
}




//Вызываю функцию вывода массива до обработки
showRes(before, arr);

//Новый массив
newArr = map(arr, fn);


//Вызываю функцию вывода массива после обработки
showRes(after, newArr);