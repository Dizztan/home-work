window.addEventListener("DOMContentLoaded", () => {

  var a = '', sign = '', b = '', result, lastRes, savedValue = '', count = 0;

  const div = document.querySelector(".keys"),
  display = document.querySelector(".display > input"),
  span = document.createElement('span');
  span.setAttribute('id', 'memShow');
  document.querySelector('.display').append(span);



  const btnEquals = document.querySelector('.block');
  btnEquals.disabled = true;
  btnEquals.classList.remove("orange");
  //событие
  div.addEventListener("click", function (e) {
    let input = e.target.value;

    //Функции записи введеных данных в переменные и  вывода их на экран.
    const aValue = () => {
      a += input;
      show(a, display);
    }

    const bValue = () => {
      b += input;
      show(b, display)
    }

    //Если введено не число
    if (isNaN(input) && input !== '.') {

      if (b !== '' && e.target.classList.contains('pink')) {
        equals();
      }

      if (savedValue !== '' && e.target.classList.contains('pink') && b!=='') {
        a = savedValue;
      }

      input === 'C' ? reset() :
        input === 'm+' ? sumMemory() :
          input == "m-" ? subtractionMemory() :
            input == 'mrc' ? showMRC() :
              input === "=" ? equalsBtn() : sign = input;

    }


    //Если введено число , "." или 0.
    if (parseInt(input) || input === '.' || input === '0') {

      if (a == '' && input === '.') {
        a += `0${input}`;
        show(a, display);
      } else if (sign != '' && lastRes != undefined && a == '') {
        a = lastRes;
        bValue();
      } else if (sign == '' || a == '') {
        aValue();
      }else if (sign != '' && lastRes == undefined){
        bValue()
      } else if (sign != '' && result != undefined) {
        bValue()
        a = result;
      } else if (sign != '') {
        bValue();
      }
    }


    //Активирую знак =.
    if (a != '' && b != '') {
      btnEquals.disabled = false;
      btnEquals.classList.add("orange");
    } else {
      btnEquals.disabled = true;
      btnEquals.classList.remove("orange");
    }
  })



  //Вывод на экран
  function show(value, el) {
    el.value = value;
  }

  //Функция сброса
  const reset = () => {
    a = '';
    b = '';
    sign = '';
    result = undefined;
    lastRes = undefined;
    show(a, display);
    btnEquals.disabled = true;
    count = 0;
  }

  //проверка знака и вызов функции мат операции.
  const exam = () => {
    switch (sign) {
      case '+':
        //a и b = строке перевожу в числовое значение
        calculate((+a), (+b), add);
        break;

      case "*":
        calculate(a, b, multiple);
        break;

      case "-":
        calculate(a, b, subtraction);
        break;

      case "/":
        calculate(a, b, division);
        break;
    }
  }


  //Функция сложения.
  const add = (a, b) => {
    return a + b;
  }

  //Функция умножения.
  const multiple = (a, b) => {
    return a * b;
  }

  //Функция деления.
  const division = (a, b) => {
    if (a == 0 || b == 0) {
      return alert("ошибка, деление с 0");
    } else {
      return a / b;
    }
  }


  //Функция вычитания.
  const subtraction = (a, b) => {
    return a - b;
  }


  function calculate(a, b, callback) {
    result = callback(a, b);
    if (result == undefined) {
      result = '';
      reset();
    }
  }

  //Функция активирует знак = 
  const equalsBtn = () => {
    exam();
    show(result, display);
    a = result;
    lastRes = result;
    result = undefined;
  }


  const equals = () => {
    exam();
    a = result;
    show(a, display);
    b = '';
  }

  // M+ - прибавить к числу из памяти число, отображенное на дисплее и результат записать в память вместо предыдущего.
  const sumMemory = () => {
    span.innerText = 'm';
    if (savedValue == '' && b === '') {
      savedValue = display.value;
    } else if (lastRes != undefined) {
      savedValue = (+savedValue) + (+lastRes);
    } else if (b === '') {
      savedValue = (+savedValue) + (+a);
    } else {
      exam();
      savedValue = result + (+savedValue);
    }
    show(savedValue,display)
    a = '';
    b = '';
    sign = '';

  }

  //M- - вычесть из числа в памяти число, отображенное на дисплее калькулятора и результат записать в память. 
  const subtractionMemory = () => {
    span.innerText = 'm';
    if(a !== "" && b!== "" && result == undefined){
      exam();
      savedValue = result;
    }else if (savedValue == '' && b === '') {
      savedValue = display.value;
    } else if (lastRes != undefined) {
      savedValue = (+savedValue) - (+lastRes);
    } else if (b === '') {
      savedValue = (+savedValue) - (+a);
    } else {
      exam();
      savedValue = (+savedValue) - result;
    }
    show(savedValue,display)
    a = '';
    b = '';
    sign = '';
  }



  const showMRC = () => {
    savedValue === undefined ? savedValue = 0 : savedValue = savedValue;
    show(savedValue, display);
    count++;
    if (count == 2) {
      savedValue = '';
      span.innerText = "";
      display.value = '';
      count = 0;
    }
  }

})
