//Переменные
  let priceShow;

  div = document.querySelector('.grid'),
  span = document.createElement("span"),
  priceSizeChoise = document.querySelector('.price__size__choice');


var pattern = true;

const priceCake =  {
  small : '250',
   mid  : '320', 
   big  : '450'
}





//Беру блок в котором кнопки и вешаю событие click для отображения цены.
document.querySelector("#pizza")
  .addEventListener('click', (e) => {
        if ( e.target.value === "small" || e.target.value === "mid" || e.target.value === "big"  ) {
          priceShow = (`${priceCake[e.target.value]}`);
          priceSizeChoise.innerText = ` ${priceShow} грн`;
        }
  })

//Убегающая при наведении скидка
//беру банер и вешаю событие
document.querySelector('#banner')
  .addEventListener('mouseover', (e) => {
    e.target.style.position = 'releative';
    e.target.style.bottom = Math.floor(Math.random() * 85) + `vh`
    e.target.style.right = Math.floor(Math.random() * 90) + `vw`
  })

//Функция проверки введенных данных,запускается после загрузки DOM.
const init = (event) => {

  //При запуске функции перебираем все формы на странице
  for (let i = 0; i < document.forms.length; i++) {

    //Записываем каждую форму в переменную "forms".
    const form = document.forms[i];

    //создаем флаг для запуска валидации форм . чтобы подстраховать себя перед отправкой данных.
    let formValidation = false;

    //Перебираем инпуты в i-ой форме
    for (let j = 0; j < form.elements.length; j++) {

      //записываем перебираемый инпут[j] в переменную input
      const input = form.elements[j];

      //Отфильтровываем инпуты по типу . нужны только text, tel и email.
      if (input.type !== 'text' && input.type !== 'tel' && input.type !== 'email') {
        continue;
      }

      //На отфильтрованые инпуты вешаю события чтобы проверять после изменений.
      if (pattern) {
        input.onchange = validateInput;
        //Меняем значение флага , по которому ориентируется функция проверки Форм перед отправкой.
        formValidation = true;
      }
    }
    //Если флаг сработал проверяем форму.
    if (formValidation) {
      form.addEventListener('submit', validateForm);// Обработчик формы на submit.В HTML изменил type = submit.
    }
  }
}
//после загрузки DOM дерева візівается функция валидации инпутов и форм.
window.addEventListener("DOMContentLoaded", init);

//вешаю события на весь див с инпутами для проверки и ограничения ввода символов с клавиатуры.
div.addEventListener('keypress', (e) => {
  //Ввод только цифр
  if (e.target.type === 'text') {
    if (e.key.match(/[0-9 ]/)) return e.preventDefault();
  }
  //Ввод только букв
  if (e.target.type === 'tel') {
    if (e.key.match(/[A-zА-яіїєґҐЄ!"№;%:?*_-]/)) return e.preventDefault();
  }
  if (e.target.type === 'email') {
    if (e.key.match(/[А-яіїєґҐЄЇІ']/)) return e.preventDefault();
  }
})

//функция проверки инпутов
function validateInput(event) {
  switch (event.target.type) {
    case "text": pattern = /^[А-яіїєґҐЄЇІ']+$/; break;
    case "tel": pattern = /^\+380\d{9}$/; break;
    case "email": pattern = /^([A-z0-9_-]+\.)*[A-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/; break;
  }

  if (pattern.test(this.value)) {
    this.classList.remove("error");
  } else {
    this.classList.add("error");
  }
}

//функция проверки Форм .
function validateForm(e) {
  // флаг
  let invalid = false;
  for (let i = 0; i < this.elements.length; i++) {
    let el = this.elements[i];
    if (el.type === "text" || el.type === "tel" || el.type === "email") {
      el.addEventListener('change', validateInput);
      if (el.className === 'error' || el.value === '') {
        invalid = true;
      } else {
        //Запись в память.
        sessionStorage.setItem(el.name, el.value);
      }
    }
    if (invalid) {
      alert('форми заповнені з помилками')
      //отмена отправки 
      e.preventDefault();
      break;
    }
  }
}


