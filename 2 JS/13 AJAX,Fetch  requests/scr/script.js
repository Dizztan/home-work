window.addEventListener('DOMContentLoaded', () => {

  //Функция запроса AJAX
  request("GET", 'https://swapi.dev/api/people/');

  //После того как пришел ответ и статус выполнен (200) выводим в консоль расшифрованые данные
  hrx.onreadystatechange = () => {
    if (hrx.readyState === 4 && hrx.status === 200) {
      /* console.log(hrx.responseText) */
      /*  console.log(JSON.parse(hrx.responseText)) */
      showInfo(JSON.parse(hrx.responseText))
      page = (JSON.parse(hrx.responseText));
      /* console.log(page); */

      //убираю спиннер
      loader.remove();

      //Деактивирую/акитвирую кнопки next и previous
      page.previous ?
        previous.classList.remove('disabled') :
        previous.classList.add('disabled');
      page.next ?
        next.classList.remove('disabled') :
        next.classList.add('disabled');

      //Ставлю флаги для next и previous чтобы при быстром нажатии номера сттраниц соответствовали своим ссылкам.
      if (flagForNext) {
        activePage = document.querySelector('ul li.active')
        if (activePage) {
          activePage.classList.remove('active');
          activePage.nextElementSibling.classList.add('active');
        }
        flagForNext = false;
      } else if (flagForPrevious) {
        activePage = document.querySelector('ul li.active')
        if (activePage) {
          activePage.classList.remove('active');
          activePage.previousElementSibling.classList.add('active');
        }
        flagForPrevious = false;
      }
    }
  }
})

const create = (nameTeg) => document.createElement(nameTeg),
  //AJAX
  hrx = new XMLHttpRequest(),
  //спинер
  loader = create('div');
loader.classList.add('spinner-grow', 'text-secondary', 'load');
loader.setAttribute('role', 'status');

//перерменные
var page, numberPage, previous, next, activePage,
  countPage, container, flagForNext = false, flagForPrevious = false, films = '', loaderFilms;

//создаю список 
const nav = create('nav'),
  pagination = create('ul');
//задаю класс
pagination.classList.add('pagination', 'justify-content-center');
nav.classList.add('mt-3');

//создаю кнопки
//запрос fetch используя sync,await чтобы получить count для расчета количества страниц
async function fetchForBtnsPage() {
  const response = await fetch('https://swapi.dev/api/people/')
  const data = await response.json()

  //Кнопки навигации
  countPage = Math.ceil(data.count / 10 + 1); //(82/10) = 8.2 округляю до 9 .+1 для кнопки next. 10-кол-во карт для вывода.
  for (let i = 0; i <= countPage; i++) {
    //Создаю кнопку previous, задаю стили , добавляю текст вешаю событие.
    if (i == 0) {
      previous = create('li');
      previous.classList.add('page-item', 'page-link');
      previous.innerText = 'Previous';
      previous.addEventListener('click', () => {
        //вызов функции запроса AJAX
        request("GET", page.previous)
        flagForPrevious = true;
      })
      pagination.prepend(previous);

      continue
      //next
    } else if (i == countPage) {
      next = create('li');
      next.classList.add('page-item', 'page-link');
      next.innerText = 'Next';
      pagination.append(next);
      next.addEventListener('click', () => {
        flagForNext = true;
        request("GET", page.next)
      })
      continue
    }

    //Кнопки с номером страниц
    numberPage = create('li');
    numberPage.classList.add('page-item', 'page-link');
    numberPage.innerText = i;
    pagination.appendChild(numberPage);
    if (numberPage.innerText === '1') {
      numberPage.classList.add('active');
    }
    numberPage.addEventListener('click', (e) => {
      request("GET", `https://swapi.dev/api/people/?page=${i}`)
      activePage = document.querySelector('ul li.active')
      if (activePage) {
        activePage.classList.remove('active');
      }
      e.target.classList.add('active');
    })
  }
}

//запуск fetch
fetchForBtnsPage();

//Функция создания и запроса AJAX
function request(get, pageLoad) {
  //Cоздаем запрос
  hrx.open(get, pageLoad);
  //Отправляем запрос
  hrx.send();
  //запуск спиннера
  document.body.append(loader);
}

//создаю и заполняю карточки
function showInfo(data) {

  //Создаю общий контейнер
  container = create('section');

  //Перебираю распарсеный запрос
  data.results.forEach((el) => {

    //создаю теги 
    const
      divCard = create('div'),
      image = create('img'),
      divBody = create('div'),
      nameHeroes = create('h5'),
      planet = create('h5'),
      discript = create('p'),
      filmsTitle = create('h5');

    //классы bootstrap
    container.classList.add('main-content', 'container');
    divCard.classList.add('card');
    divCard.style.width = ('18rem');
    divCard.style.minHeight = ('10rem');
    divBody.classList.add('card-body');
    nameHeroes.classList.add('card-title');
    planet.classList.add('card-subtitle', 'mb-2', 'text-muted');
    discript.classList.add('card-text', 'd-none');
    filmsTitle.classList.add('d-none');
    image.classList.add('card-img-top')

    //Запись данных   
    //Беру число элемента по ссылке
    let numberImage = el.url.match(/\d+/g);

    //ищу фото соответсвующее герою с помощью числа.
    image.setAttribute('src', `https://starwars-visualguide.com/assets/img/characters/${numberImage}.jpg`)

    nameHeroes.innerText = el.name;
    //Для вывода планеты создаю новый запрос.
    if (el.homeworld) {
      let loaderPlanet = create('span');
      loaderPlanet.classList.add('spinner-border', 'spinner-border-sm');
      loaderPlanet.setAttribute('role', 'status');
      planet.appendChild(loaderPlanet);
      const worldHeroes = new XMLHttpRequest();
      worldHeroes.open('GET', el.homeworld);
      worldHeroes.send();
      worldHeroes.onreadystatechange = () => {
        if (worldHeroes.readyState === 4 && worldHeroes.status === 200) {
          world = (JSON.parse(worldHeroes.responseText))
          planet.innerText = `From: ${world.name}`;

          /* //Вывод в консоль 
          console.log(JSON.parse(worldHeroes.responseText));
          console.log(world.name); */
        }
      }
    }

    //Заполнение карточки данными из запроса
    discript.innerText = `Gender: ${el.gender}. 
    Birth year: ${el.birth_year}.
    Height: ${el.height}.
    Mass: ${el.mass}.
    Hair color: ${el.hair_color}.
    Eye color: ${el.eye_color}.
    Skin color: ${el.skin_color}.`;

    //вывод заполненных елементов.
    container.append(divCard);
    divCard.prepend(image);
    divCard.append(divBody);
    divBody.append(nameHeroes, planet, discript);
    discript.after(filmsTitle);

    //функция отмены события после клика
    function handleClick() {
      divCard.removeEventListener("click", handleClick);

      //вывод описания после нажатия
      discript.classList.remove('d-none');
      filmsTitle.innerText = 'Films: ';
      filmsTitle.classList.remove('d-none');

      //Перебираю массив с сылками и каждую отправляю в асинхронную функцию с фетч запросом
      el.films.forEach((element) => {
        FilmsSearch(element, filmsTitle)
      })

      loaderFilms = create('span');
      loaderFilms.classList.add('spinner-grow', 'spinner-grow-sm');
      loaderFilms.setAttribute('role', 'status');
      filmsTitle.appendChild(loaderFilms);
    };

    //Событие на карточки
    divCard.addEventListener('click', (handleClick));
  })

  async function FilmsSearch(url, filmsTitle) {
    const response = await fetch(url);
    films = await response.json();
    filmName = create('h6');
    filmName.classList.add('m-0',)
    filmName.innerText = films.title;
    filmsTitle.after(filmName);
    loaderFilms.remove();
    /*  console.log(films.title); */
  }

  //вывод на экран
  //Удаляю первый элемент на странице (container)
  document.body.firstChild.remove();
  //Вставляю container первым на страницу
  document.body.prepend(container);
  //добавляю кнопки 
  container.after(nav);
  nav.append(pagination);
}