//Массив

/* function importAll(item) {
  return item.keys().map(item);
}
const images = importAll(require.context('./images', true, /\.(png|jpe?g|svg)$/)); */



//Объект (ключ -  путь файла)
const reqImages = require.context ('./images', true, /\.(png|jpe?g|svg)$/);

const items = reqImages
  .keys ()
  .reduce ( ( el, path ) => {
    el[path] = reqImages (path);
    return el
  }, {} )

  console.dir(items);

export default items;