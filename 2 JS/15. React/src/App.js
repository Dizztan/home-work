import './App.css'; 
//импорт объекта с картинками
import images from './image';


function App() {
  return (
    <ul className='zodiac__ul'>
      <h1>Знаки зодиака</h1>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%9E%D0%B2%D0%B5%D0%BD_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Овен</h2>
       <img src= {images['./Aries.jpg']} alt='Aries'/></a>
      </li>
      <li> <a href='https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D0%BB%D0%B5%D1%86_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Телец</h2>
        <img src={images['./taurus.jpg']} alt='Taurus'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%91%D0%BB%D0%B8%D0%B7%D0%BD%D0%B5%D1%86%D1%8B_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Близнецы</h2>
        <img src={images['./Twins.jpg']} alt='Twins'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%BA_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target = {'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Рак</h2>
        <img src={images['./cryfish.jpg']} alt='Cryfish'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%9B%D0%B5%D0%B2_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Лев</h2>
        <img src={images['./lion.jpg']} alt='Lion'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D0%B2%D0%B0_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Дева</h2>
        <img src={images['./Virgo.jpg']} alt='Virgo'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%92%D0%B5%D1%81%D1%8B_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Весы</h2>
        <img src={images['./scales.jpg']} alt='Scales'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%A1%D0%BA%D0%BE%D1%80%D0%BF%D0%B8%D0%BE%D0%BD_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Скорпион</h2>
        <img src={images['./scorpion.jpg']} alt='Scorpion'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%97%D0%BC%D0%B5%D0%B5%D0%BD%D0%BE%D1%81%D0%B5%D1%86_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Змееносец</h2>
        <img src={images['./Ophiuchus.jpg']} alt='Ophiuchus'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D1%80%D0%B5%D0%BB%D0%B5%D1%86_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Стрелец</h2>
        <img src={images['./Sagittarius.jpg']} alt='Sagittarius'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%B7%D0%B5%D1%80%D0%BE%D0%B3_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Козерог</h2>
        <img src={images['./Capricorn.jpg']} alt='Capricorn'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%B4%D0%BE%D0%BB%D0%B5%D0%B9_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Водолей</h2>
        <img src={images['./aquarius.jpg']} alt='Aquarius'/></a>
      </li>
      <li><a href='https://ru.wikipedia.org/wiki/%D0%A0%D1%8B%D0%B1%D1%8B_(%D0%B7%D0%BD%D0%B0%D0%BA_%D0%B7%D0%BE%D0%B4%D0%B8%D0%B0%D0%BA%D0%B0)' target={'_blank'} rel={'noreferrer'}>
        <h2 className = 'gradient'>Рыбы</h2>
        <img src={images['./fish.jpg']} alt='Fish'/></a>
      </li>
    </ul>
  );
}
export default App;
