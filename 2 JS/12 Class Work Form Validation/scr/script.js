/*
Перевірка даних.

        1. Використовуючи JS, створіть 5 полів для води даних і залиште їх. 
        2. Поля:
        Ім'я (Укр. Літери), Номер телефону у форматі +38ХХХ-ХХХ-ХХ-ХХ, електронна пошта, пароль та підтвердження пароля (паролі повинні співпадати).
        
        3. Додайте стилі на помилку і стиль на правильне введення.
        4. Реалізуй перевірку даних.
        При введенні даних відразу перевіряти на правильність і виводити помилку, якщо така необхідна.
        додай кнопку реєстрація, при натисканні кнопки перевірити всі поля та вивести помилку якщо така буде.
        
        Збереження.
        5. Якщо користувач все записав правильно, то збережіть дані на клієнті із зазначенням дати та часу збереження.
        
        Для стилізації використовуй CSS класи. Для створення елементів використовуй JS.
*/




window.addEventListener("DOMContentLoaded", () => {

  const form = document.createElement('form'),
    span = document.createElement('span');
  span.classList.add('spanError');

  document.body.prepend(form);
  form.prepend(inputs.name, inputs.phone, inputs.email, inputs.password, inputs.passwordRepeat, inputs.registration);

  document.querySelectorAll("input")
    .forEach((el) => {
      if (el.type !== 'button') {

        el.addEventListener("change", (e) => {
          console.log(validate(e));
          if (validate(e)) {
            el.classList.add('correct');
            el.classList.remove('error');
            span.remove();

          } else {
            el.classList.add('error');
            el.classList.remove('correct');
            //Подсказки при неправильном вводе
            if (el.type === 'tel') {
              span.innerText = 'Enter the number in the format: +38012-345-67-89';
            } else if (el.type === 'email') {
              span.innerText = `Email address entered incorrectly`;
            } else if (el.placeholder === 'Repeat password') {
              span.innerText = `The entered passwords do not match`
            } else {
              span.innerText = `Enter name using сyrillic`;
            }
            el.after(span);
          }
        });
      } else {
        //Проверка формы по нажатию кнопки 
        el.addEventListener('click', () => {
          let invalid = false;
          for (let i = 0; i < form.elements.length; i++) {
            let el = form.elements[i];
            if (el.type !== 'button') {
              el.addEventListener('change', validate);
              if (el.className === 'error' || el.value === '') {
                invalid = true;
              } else {
                //Запись в память.
                const dateRegistration = new Date();
                if (el.type === 'text') {
                  sessionStorage.setItem('Name', el.value);
                  continue
                }
                sessionStorage.setItem(el.type, el.value);
                sessionStorage.setItem('Date', dateRegistration.toLocaleDateString() + dateRegistration.toLocaleTimeString());
              }
            }
            if (invalid) {
              alert('Fields filled out incorrectly')
              break;
            }
          }
        });
      }
    })
});

// create html input
function cretaeInput(type = "", className = "", placeholder = "") {
  const input = document.createElement("input"),
    div = document.createElement("div"),
    label = document.createElement("label"),
    p = document.createElement("p"),
    id = `input${Math.random() * 1000000}`;

  input.id = id;
  label.setAttribute("for", id);
  input.type = type;
  input.className = className;
  label.innerText = placeholder
  div.prepend(label)
  label.after(input)
  div.append(p)


  if (type === "button") {
    input.value = placeholder
    label.remove();
  } else {
    input.placeholder = placeholder
  }
  return div
}

// стврення інпутів. 
const inputs = {
  name: cretaeInput("text", "user-info", "input your name"),
  phone: cretaeInput("tel", "user-info", "input your phone"),
  email: cretaeInput("email", "user-info", "input your email"),
  password: cretaeInput("password", "user-info", "input your password"),
  passwordRepeat: cretaeInput("password", "user-info", "Repeat password"),
  //кнопка регистрации
  registration: cretaeInput('button', 'registr', 'Registration')
}



//проверка пароля

function validatePassword() {
  const passwordField = document.querySelectorAll(`input[type="password"]`)[0],
    passwordFieldRepeat = document.querySelectorAll(`input[type="password"]`)[1];

  if (passwordField.value !== '' && passwordFieldRepeat.value === '') {
    return /^([A-z])\w+$/.test(passwordField.value);
  } else if (passwordField.value !== '' && passwordFieldRepeat.value !== '') {
    return passwordField.value === passwordFieldRepeat.value;
  }
}


//перевірка даних

function validate(event) {
  switch (event.target.type) {
    case "text": return /^[А-яіґєїІҐЄЇ']+$/.test(event.target.value);
    case "tel": return /^\+380\d{2}-\d{3}-\d{2}-\d{2}$/.test(event.target.value);
    case "email": return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(event.target.value)
    case "password": return validatePassword();
  }
}


