let seconds = 0, minutes = 0, hours = 0, secondsHandler, minutesHandler, hoursHandler;

//Функция get (чтоб не писать каждый раз get element...)
const get = (id) => document.getElementById(id);

const secondsField = get("seconds");
const minutesField = get("minutes");
const hoursField = get("hours");

const timerColor = get("timer");


const timerSeconds = () => {
  seconds++;
  seconds > 59 ? secondsField.innerText = `0${seconds = 0}` :
    seconds > 9 ? secondsField.innerText = seconds : secondsField.innerText = `0${seconds}`;

}

const timerMinutes = () => {
  minutes++;
  minutes > 59 ? minutesField.innerText = `0${minutes = 0}` :
    minutes > 9 ? minutesField.innerText = minutes : minutesField.innerText = `0${minutes}`;
}


const timerHours = () => {
  hours++;
  hours > 23 ? hoursField.innerText = `0${hours = 0}` :
    hours > 9 ? hoursField.innerText = hours : hoursField.innerText = `0${hours}`;
}



//По id беру кнопку и вешаю событие старт
get("startBtn").onclick = () => {
  //запускаю секундомер.
  hoursHandler = setInterval(timerHours, 3600000);
  minutesHandler = setInterval(timerMinutes, 60000);
  secondsHandler = setInterval(timerSeconds, 1000);

  //Меняю цвет.
  timerColor.classList.add("green");
  timerColor.classList.remove("black");

  //деактивирую кнопку старт.
  get("startBtn").disabled = true;
}


//Остановка таймера
const stopTimer = () => {
  clearInterval(secondsHandler);
  clearInterval(minutesHandler);
  clearInterval(hoursHandler);
}

//стоп
get("stopBtn").onclick = () => {
  //Останавливаю секундомер
  stopTimer();
  //Переключаю цвет
  timerColor.classList.add("red");
  timerColor.classList.remove("green");
  //Активирую кнопку старт.
  get("startBtn").disabled = false;
}


const resetFields = () => {
  secondsField.innerText = `00`;
  minutesField.innerText = `00`;
  hoursField.innerText = `00`;
}

//сброс 
get("resetBtn").onclick = () => {

  seconds = 0;
  minutes = 0;
  hours = 0;

  timerColor.classList.add("silver");
  timerColor.classList.remove("red");
  timerColor.classList.remove("green");

  resetFields();
  stopTimer();
  get("startBtn").disabled = false;
}

